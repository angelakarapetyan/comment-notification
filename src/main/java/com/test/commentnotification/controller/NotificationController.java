package com.test.commentnotification.controller;

import com.test.commentnotification.service.NotificationService;
import com.test.commentnotification.service.criteria.SearchCriteria;
import com.test.commentnotification.service.dto.NotificationDto;
import com.test.commentnotification.service.model.QueryResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/notifications")
public class NotificationController {

    private final NotificationService notificationService;

    @Autowired
    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }


    @GetMapping
    public QueryResponseWrapper<NotificationDto> getNotifications(SearchCriteria searchCriteria) {
        return notificationService.getNotifications(searchCriteria);
    }
}
