package com.test.commentnotification.controller;

import com.test.commentnotification.exception.RequestParamMissingException;
import com.test.commentnotification.service.CommentService;
import com.test.commentnotification.service.criteria.SearchCriteria;
import com.test.commentnotification.service.dto.CommentDto;
import com.test.commentnotification.service.model.QueryResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comments")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }


    @PostMapping
    public CommentDto createComment(@RequestBody CommentDto commentDto) throws InterruptedException {
        if (commentDto.getContent() == null || commentDto.getContent().trim().equals("")) {
            throw new RequestParamMissingException("content");
        }

        return commentService.createCommentWithNotification(commentDto);
    }

    @GetMapping
    public QueryResponseWrapper<CommentDto> getComments(SearchCriteria searchCriteria) {
        return commentService.getComments(searchCriteria);
    }
}
