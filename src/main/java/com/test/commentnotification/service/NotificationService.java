package com.test.commentnotification.service;

import com.test.commentnotification.persistence.entity.CommentEntity;
import com.test.commentnotification.persistence.entity.NotificationEntity;
import com.test.commentnotification.persistence.repository.NotificationRepository;
import com.test.commentnotification.service.criteria.SearchCriteria;
import com.test.commentnotification.service.dto.NotificationDto;
import com.test.commentnotification.service.model.QueryResponseWrapper;
import com.test.commentnotification.util.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;

import static com.test.commentnotification.util.UtilService.sleepAndRandomThrowRuntimeException;

@Service
public class NotificationService {

    private final NotificationRepository notificationRepository;
    private final MapperService mapperService;

    @Autowired
    public NotificationService(NotificationRepository notificationRepository,
                               MapperService mapperService) {
        this.notificationRepository = notificationRepository;
        this.mapperService = mapperService;
    }

    public void createNotification(CommentEntity commentEntity) {
        NotificationEntity notificationEntity = new NotificationEntity();
        notificationEntity.setCommentEntity(commentEntity);
        notificationEntity.setTime(new Date());
        notificationEntity.setDelivered(true);

        notificationRepository.save(notificationEntity);

        try {
            doSomeWorkOnNotification();
        } catch (Exception e) {
            notificationEntity.setDelivered(false);
        }
    }

    public QueryResponseWrapper<NotificationDto> getNotifications(SearchCriteria searchCriteria) {
        PageRequest pageRequest = UtilService.composePageRequest(searchCriteria);
        Page<NotificationEntity> notificationEntities = notificationRepository.findAll(pageRequest);

        return new QueryResponseWrapper<>(notificationEntities.getTotalElements(),
                mapperService.mapNotificationEntitiesToDtos(notificationEntities.getContent()));
    }


    public static void doSomeWorkOnNotification() {
        sleepAndRandomThrowRuntimeException(2, 10);
    }
}
