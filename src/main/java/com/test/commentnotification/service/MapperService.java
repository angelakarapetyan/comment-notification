package com.test.commentnotification.service;

import com.test.commentnotification.persistence.entity.CommentEntity;
import com.test.commentnotification.persistence.entity.NotificationEntity;
import com.test.commentnotification.service.dto.CommentDto;
import com.test.commentnotification.service.dto.NotificationDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MapperService {

    private final ModelMapper modelMapper;

    @Autowired
    public MapperService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public CommentEntity mapCommentDtoToEntity(CommentDto commentDto) {
        return modelMapper.map(commentDto, CommentEntity.class);
    }

    public CommentDto mapCommentEntityToDto(CommentEntity commentEntity) {
        return modelMapper.map(commentEntity, CommentDto.class);
    }

    List<CommentDto> mapCommentEntitiesToDtos(List<CommentEntity> commentEntities) {
        List<CommentDto> commentDtos = commentEntities.stream().map(this::mapCommentEntityToDto)
                .collect(Collectors.toList());

        return commentDtos;
    }

    public NotificationEntity mapNotificationDtoToEntity(NotificationDto notificationDto) {
        return modelMapper.map(notificationDto, NotificationEntity.class);
    }

    public NotificationDto mapNotificationEntityToDto(NotificationEntity notificationEntity) {
        return modelMapper.map(notificationEntity, NotificationDto.class);
    }

    public List<NotificationDto> mapNotificationEntitiesToDtos(List<NotificationEntity> notificationEntities) {
        List<NotificationDto> notificationDtos = notificationEntities.stream()
                .map(this::mapNotificationEntityToDto).collect(Collectors.toList());

        return notificationDtos;
    }
}
