package com.test.commentnotification.service.dto;

import java.util.Date;

public class CommentDto {
    private Long id;
    private String content;
    private Date time;
    private NotificationDto notificationDto;

    public CommentDto() {}

    public CommentDto(String content, Date time) {
        this.content = content;
        this.time = time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public NotificationDto getNotificationDto() {
        return notificationDto;
    }

    public void setNotificationDto(NotificationDto notificationDto) {
        this.notificationDto = notificationDto;
    }
}
