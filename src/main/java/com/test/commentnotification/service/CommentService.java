package com.test.commentnotification.service;

import com.test.commentnotification.exception.CustomException;
import com.test.commentnotification.persistence.entity.CommentEntity;
import com.test.commentnotification.persistence.repository.CommentRepository;
import com.test.commentnotification.service.criteria.SearchCriteria;
import com.test.commentnotification.service.dto.CommentDto;
import com.test.commentnotification.service.model.QueryResponseWrapper;
import com.test.commentnotification.util.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static com.test.commentnotification.exception.ErrCodes.ERR_FAILURE_COMMENT_CREATION;
import static com.test.commentnotification.util.UtilService.sleepAndRandomThrowRuntimeException;

@Service
public class CommentService {

    private final CommentRepository commentRepository;
    private final MapperService mapperService;
    private final NotificationService notificationService;

    @Autowired
    public CommentService(CommentRepository commentRepository, MapperService mapperService,
                          NotificationService notificationService) {
        this.commentRepository = commentRepository;
        this.mapperService = mapperService;
        this.notificationService = notificationService;
    }


    @Transactional(rollbackFor = {Exception.class}, timeout = 2)
    public CommentDto createCommentWithNotification(CommentDto commentDto) {
        try {
            CommentEntity commentEntity = createComment(commentDto);
            commentDto.setId(commentEntity.getId());

            doSomeWorkOnCommentCreation();

            notificationService.createNotification(commentEntity);

            return commentDto;

        } catch (Exception e) {
            throw new CustomException(ERR_FAILURE_COMMENT_CREATION, "Failure on comment creation");
        }
    }

    private CommentEntity createComment(CommentDto commentDto) {
        commentDto.setTime(new Date());
        CommentEntity commentEntity = mapperService.mapCommentDtoToEntity(commentDto);
        commentEntity = commentRepository.save(commentEntity);
        System.out.println(commentEntity.getId());

        return commentEntity;
    }

    public QueryResponseWrapper<CommentDto> getComments(SearchCriteria searchCriteria) {
        PageRequest pageRequest = UtilService.composePageRequest(searchCriteria);

        Page<CommentEntity> commentEntities = commentRepository.findAll(pageRequest);
        return new QueryResponseWrapper<>(commentEntities.getTotalElements(),
                mapperService.mapCommentEntitiesToDtos(commentEntities.getContent()));
    }

    public static void doSomeWorkOnCommentCreation() {
        sleepAndRandomThrowRuntimeException(1, 30);
    }
}
