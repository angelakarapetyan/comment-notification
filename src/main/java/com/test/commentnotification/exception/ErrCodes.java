package com.test.commentnotification.exception;

public class ErrCodes {
    public static final String ERR_PARAM_MISSING = "ERR_001";
    public static final String ERR_FAILURE_COMMENT_CREATION = "ERR_002";
    public static final String ERR_TIMEOUT = "ERR_003";
}
