package com.test.commentnotification.exception;

public class CustomException extends RuntimeException {

    private String code;
    private String message;

    public CustomException(String code, String message) {
        super(message);
        this.setCode(code);
        this.setMessage(message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}