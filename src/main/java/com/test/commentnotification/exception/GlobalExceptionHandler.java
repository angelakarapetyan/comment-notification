package com.test.commentnotification.exception;

import org.hibernate.TransactionException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({RequestParamMissingException.class})
    @ResponseBody
    public ResponseError handleRequestParamMissingException(RequestParamMissingException e) {
        return ResponseError.createError(e);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler({CustomException.class})
    @ResponseBody
    public ResponseError handleCustomException(CustomException e) {
        return ResponseError.createError(e);
    }

    @ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
    @ExceptionHandler({TransactionException.class})
    @ResponseBody
    public ResponseError handleTransactionException(TransactionException e) {
        return new ResponseError(ErrCodes.ERR_TIMEOUT, "Timeout exception");
    }
}
