package com.test.commentnotification.exception;

public class RequestParamMissingException extends CustomException {

    public RequestParamMissingException(String paramName) {
        super(ErrCodes.ERR_PARAM_MISSING, String.format("Param '%s' is required", paramName));
    }
}