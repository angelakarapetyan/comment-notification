package com.test.commentnotification.util;

import com.test.commentnotification.service.criteria.SearchCriteria;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class UtilService {

    public static PageRequest composePageRequest(SearchCriteria searchCriteria) {
        return PageRequest.of(searchCriteria.getPage() == null ? 0 : searchCriteria.getPage(),
                searchCriteria.getSize() == null ? 10 : searchCriteria.getSize());
    }

    public static void sleepAndRandomThrowRuntimeException(int seconds, int
            exceptionProbabilityProc) {
        try {
            Thread.sleep((long) (seconds * 1000 * Math.random()));
        } catch (InterruptedException e) {
            //do nothing
        }
        int randomProc = (int) (100 * Math.random());
        if (exceptionProbabilityProc > randomProc) throw new RuntimeException();
    }
}
