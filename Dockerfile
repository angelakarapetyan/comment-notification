FROM openjdk:8
ADD target/comment-notification-0.0.1-SNAPSHOT.jar comment-notification-0.0.1-SNAPSHOT.jar
EXPOSE 9000
ENTRYPOINT ["java","-jar","comment-notification-0.0.1-SNAPSHOT.jar"]